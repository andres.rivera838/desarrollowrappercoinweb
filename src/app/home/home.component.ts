import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ReactiveFormsModule, FormArray } from '@angular/forms';
import { UtilsService } from '../services/utils.service'
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  form: any;

  btc: any = {
    status: true,
    value: 13197
  };

  usd: any = {
    status: false,
    value: 1
  };
  total: number = 0;

  list: any[] = [
    {
      type: "usd",
      value: 300
    },
    {
      type: "usd",
      value: 500
    },
    {
      type: "btc",
      value: 3
    },
    {
      type: "btc",
      value: 1
    },
    {
      type: "usd",
      value: 300
    }
  ];

  constructor(private formBuilder: FormBuilder, private utilsService: UtilsService) {
    this.form = this.formBuilder.group(
      {
        "amount": ['', Validators.required]
      }
    );

  }

  ngOnInit(): void {
    this.consumPost();
  }

  async consumPost(){
    try {
      let rs = await this.utilsService.eventPost('api/user', {}).toPromise();
      console.log("HomeComponent -> consumPost -> rs", rs)
    } catch (error) {
      console.log("HomeComponent -> consumPost -> error", error)
      
    }
  }

  eventTotal() {
    if (!!this.btc.status) {
      this.total = this.form.value.amount / this.btc.value
    } else {
      this.total = this.form.value.amount * this.btc.value
    }
    console.log("HomeComponent -> eventTotal -> this.total", this.total)
  }

  changeMoney() {
    this.form.get('amount').setValue(this.total);
    if (!!this.btc.status) {
      this.usd.status = true;
      this.btc.status = false;
    } else {
      this.usd.status = false;
      this.btc.status = true;
    }
    this.eventTotal();
  }

  exchange(data) {
    console.log("HomeComponent -> exchange -> data", data)
    this.form.get('amount').setValue(parseInt(data));
    if (data.type === 'btc') {
      this.usd.status = true;
      this.btc.status = false;
    } else {
      this.usd.status = false;
      this.btc.status = true;
    }
  }

}
